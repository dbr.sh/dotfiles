" -- Enlarge Cache -- "
set hidden
set history=100

" -- Colors and Theming -- " 
filetype on
syntax enable
"let g:sierra_Twilight=1
colorscheme sierra

" -- Numbering -- "
set number

" -- Indentation -- "
set nowrap 
set tabstop=2
set shiftwidth=2
set expandtab

" -- Get Backspace Working  -- "
set backspace=indent,eol,start

" -- Background tag characters over 80 columns -- "
highlight OverLength ctermbg=red ctermfg=white guibg=#592929
match OverLength /\%>80v.\+/
